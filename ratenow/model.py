print('init model')
from datetime import datetime
import requests


def rate_now():
    url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'
    try:
        rate_response = requests.get(url)
    except:
        raise ValueError('Wrong request url was provided or something went wrong while calling this request.')

    if rate_response.ok:
        with open('ratenow/templates/rate.html', 'w+') as r:
            r.write('''
            <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Get Rate Now</title>
</head>
<body>
            ''')
            r.write('<h1>Rate Now</h1>')
            r.write('<h2> Request was made on ' + str(datetime.now()) + '\n' + '</h2>')
            try:
                rate_res = rate_response.json()
            except:
                raise ValueError(f'Response from {url} was received not in JSON format.')
            try:
                for rate in rate_res:
                    r.write(f'<h4>{rate["cc"]} to UAH: {rate["rate"]}</h4>')
                r.write('''
                </body>
</html>
                ''')
            except:
                raise ValueError('Data is not available at this moment, please try again later. If this issue persists,'
                                 'please contact support.')
    else:
        print('Unsuccessful request, please try again. If this issue persists, please contact support.')

