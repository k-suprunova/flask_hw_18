from core import app

from flask import render_template
from ratenow import model


@app.route('/ratenow')
def ratenow():
    model.rate_now()
    return render_template('rate.html')